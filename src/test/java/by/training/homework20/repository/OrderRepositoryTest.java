package by.training.homework20.repository;

import by.training.homework20.dao.OrderDao;
import by.training.homework20.entity.Order;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class OrderRepositoryTest {

    @Mock
    private OrderDao orderDao;

    @InjectMocks
    private Order order;

    @Test
    public void testGetByUserId() {
        when(orderDao.getByUserId(21)).thenReturn(order);

        Assert.assertEquals(orderDao.getByUserId(21), order);

        verify(orderDao, times(1)).getByUserId(21);
    }
}