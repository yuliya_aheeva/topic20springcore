package by.training.homework20.repository;

import by.training.homework20.dao.ProductDao;
import by.training.homework20.entity.Product;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ProductRepositoryTest {

    @Mock
    private ProductDao productDao;
    @Mock
    private List<Product> productList;

    @InjectMocks
    private Product product;

    @Before
    public void setUp() {
        productList.add(product);
    }

    @Test
    public void testGetAllProducts() {
        when(productDao.getAllProducts()).thenReturn(productList);

        assertEquals(productDao.getAllProducts(), productList);

        verify(productDao, times(1)).getAllProducts();
    }
}