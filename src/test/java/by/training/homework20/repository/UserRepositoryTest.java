package by.training.homework20.repository;

import by.training.homework20.dao.UserDao;
import by.training.homework20.entity.User;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UserRepositoryTest {

    @Mock
    private UserDao userDao;

    @InjectMocks
    private User userMock = new User();

    @Test
    public void testGetUserByName() {
        when(userDao.getUserByName("user")).thenReturn(userMock);

        Assert.assertEquals(userDao.getUserByName("user"), userMock);

        verify(userDao, times(1)).getUserByName("user");
    }

    @Test
    public void testIsUserExist() {
        when(userDao.isUserExist("user")).thenReturn(true);
        assertTrue(userDao.isUserExist("user"));

        when(userDao.isUserExist("user")).thenReturn(false);
        assertFalse(userDao.isUserExist("user"));

        verify(userDao, times(2)).isUserExist("user");
    }
}