package by.training.homework20.repository;

import by.training.homework20.dao.OrderProductDao;
import by.training.homework20.entity.Product;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class OrderProductRepositoryTest {

    @Mock
    private OrderProductDao orderProductDao;
    @Mock
    private List<Product> productList;

    @InjectMocks
    private Product product;

    @Before
    public void setUp() {
        productList.add(product);
    }

    @Test
    public void testGetTotalPrice() {
        when(orderProductDao.getTotalPrice(21)).thenReturn((double) 12);

        assertEquals(orderProductDao.getTotalPrice(21), 12.0, 1);

        verify(orderProductDao, times(1)).getTotalPrice(21);
    }

    @Test
    public void testGetChosenList() {
        when(orderProductDao.getChosenList(21)).thenReturn(productList);

        assertEquals(orderProductDao.getChosenList(21), productList);

        verify(orderProductDao, times(1)).getChosenList(21);
    }
}