package by.training.homework20.servlets;

import by.training.homework20.dao.OrderDao;
import by.training.homework20.dao.UserDao;
import by.training.homework20.entity.Order;
import by.training.homework20.entity.User;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class WelcomePageServletTest {

    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private UserDao userService;
    @Mock
    private OrderDao orderService;

    @InjectMocks
    private User user;
    @InjectMocks
    private Order order;

    @Test
    public void testWelcomeServlet() throws IOException {
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);

        when(request.getParameter("userName")).thenReturn("qwe");
        lenient().when(response.getWriter()).thenReturn(printWriter);
        when(request.getParameter("consent")).thenReturn("on");
        when(userService.isUserExist("user")).thenReturn(true);
        when(userService.getUserByName("user")).thenReturn(user);
        when(orderService.getByUserId(21)).thenReturn(order);

        Assert.assertEquals(orderService.getByUserId(21), order);
        Assert.assertEquals(userService.getUserByName("user"), user);
        assertTrue(userService.isUserExist("user"));
        assertEquals("on", request.getParameter("consent"));
        assertEquals("qwe", request.getParameter("userName"));

        when(request.getParameter("consent") == null).thenReturn(null);
        assertNull(request.getParameter("consent"));

        verify(request, never()).getParameterValues("product");
        verify(request, times(1)).getParameter("userName");
        verify(request, times(2)).getParameter("consent");
        verify(userService, times(1)).getUserByName("user");
        verify(userService, times(1)).isUserExist("user");
        verify(orderService, times(1)).getByUserId(21);
    }
}