package by.training.homework20.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.sql.Connection;
import java.sql.Statement;

import static junit.framework.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class LocalConnectDbTest {

    @Mock
    private Connection mockConnection;
    @Mock
    private Statement mockStatement;

    @Test
    public void testMockDBConnection() throws Exception {
        lenient().when(mockConnection.createStatement()).thenReturn(mockStatement);

        assertEquals(mockConnection.createStatement(), mockStatement);

        verify(mockConnection,times(1)).createStatement();
    }
}