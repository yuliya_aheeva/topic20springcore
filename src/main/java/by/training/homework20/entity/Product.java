package by.training.homework20.entity;

import java.io.Serializable;
import java.math.BigDecimal;

public class Product implements Serializable {

    private static final long serialVersionUID = 44L;

    private long id;
    private String title;
    private BigDecimal price;

    public Product(String title, BigDecimal price) {
        this.title = title;
        this.price = price;
    }

    public Product() {

    }

    public Product(String stringIdNamePrice) {
        this.id = Integer.valueOf(stringIdNamePrice.substring(0, stringIdNamePrice.indexOf(".")));
        this.title = stringIdNamePrice.substring(stringIdNamePrice.indexOf(" "), stringIdNamePrice.indexOf(","));
        this.price = BigDecimal.valueOf(Double.valueOf(stringIdNamePrice.substring(stringIdNamePrice.indexOf("$") + 1)));
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String name) {
        this.title = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return id +
                ". " + title +
                ", $" + price;
    }
}
