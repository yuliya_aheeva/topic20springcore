package by.training.homework20.entity;

import java.io.Serializable;

public class OrderProduct implements Serializable {

    private static final long serialVersionUID = 43L;

    private long id;
    private long orderId;
    private long productId;

    public OrderProduct() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getOrderId() {
        return orderId;
    }

    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }

    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }
}
