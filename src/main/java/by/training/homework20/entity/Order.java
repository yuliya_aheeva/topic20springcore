package by.training.homework20.entity;

import java.io.Serializable;
import java.math.BigDecimal;

public class Order implements Serializable {

    private static final long serialVersionUID = 43L;

    private long id;
    private long userId;
    private BigDecimal totalPrice;

    public Order() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }
}
