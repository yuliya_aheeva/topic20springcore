package by.training.homework20.servlets;

import by.training.homework20.dao.OrderProductDao;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(value = "/order", name = "order")
public class SummaryOrderServlet extends HttpServlet {

    private OrderProductDao orderProductDao;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        try (ClassPathXmlApplicationContext context =
                     new ClassPathXmlApplicationContext("applicationContext.xml")) {

            orderProductDao = context.getBean("orderProductDao", OrderProductDao.class);
        }
        HttpSession session = request.getSession();
        long userId = (Long) session.getAttribute("userId");
        double totalPrice = orderProductDao.getTotalPrice(userId);
        request.setAttribute("totalPrice", totalPrice);

        RequestDispatcher dispatcher = request.getRequestDispatcher("shopPages/order.jsp");
        dispatcher.forward(request, response);
    }
}
