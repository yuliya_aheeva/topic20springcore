package by.training.homework20.servlets;

import by.training.homework20.dao.OrderDao;
import by.training.homework20.dao.UserDao;
import by.training.homework20.entity.Order;
import by.training.homework20.entity.User;
import by.training.homework20.service.FakeProductRepository;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;


@WebServlet(value = "/login", name = "login")
public class WelcomePageServlet extends HttpServlet {

    private User user;
    private Order order;
    private String userName;
    private UserDao userDao;
    private OrderDao orderDao;

    @Override
    protected void service(HttpServletRequest request, HttpServletResponse resp) throws ServletException, IOException {

        try (ClassPathXmlApplicationContext context =
                     new ClassPathXmlApplicationContext("applicationContext.xml")) {

            FakeProductRepository fakeProductRepository =
                    context.getBean("fakeProductRepository", FakeProductRepository.class);
            fakeProductRepository.createTables();
            fakeProductRepository.addProductsToTable();

            userName = request.getParameter("userName");
            userDao = context.getBean("userDao", UserDao.class);
            orderDao = context.getBean("orderDao", OrderDao.class);
        }
        if (userDao.isUserExist(userName)) {
            user = userDao.getUserByName(userName);
            order = orderDao.getByUserId(user.getId());
        } else {
            user = new User();
            user.setName(userName);
            userDao.add(user);

            user = userDao.getUserByName(userName);
            order = new Order();
            order.setUserId(user.getId());
            orderDao.add(order);
        }
        String method = request.getMethod();
        if (method.equals("GET")) {
            doGet(request, resp);
        } else if (method.equals("POST")) {
            doPost(request, resp);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        RequestDispatcher dispatcher = request.getRequestDispatcher("shopPages/login.jsp");
        dispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        HttpSession session = request.getSession();

        if (request.getParameter("button") != null) {
            if (request.getParameter("consent") != null && userName.length() > 2) {
                session.setAttribute("consent", "on");
                session.setAttribute("userName", userName);
                response.sendRedirect("/online-shop/catalog");
            } else {
                response.sendRedirect("/online-shop/oops");
            }
        }
    }
}
