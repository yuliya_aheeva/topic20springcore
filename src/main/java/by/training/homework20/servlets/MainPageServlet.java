package by.training.homework20.servlets;

import by.training.homework20.dao.OrderDao;
import by.training.homework20.dao.OrderProductDao;
import by.training.homework20.dao.UserDao;
import by.training.homework20.entity.Order;
import by.training.homework20.entity.Product;
import by.training.homework20.entity.User;
import by.training.homework20.service.ProductService;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(value = "/catalog", name = "catalog")
public class MainPageServlet extends HttpServlet {

    private User user;
    private Product product;
    private Order order;
    private HttpSession session;
    private double totalPrice;
    private OrderProductDao orderProductDao;
    private OrderDao orderDao;
    private UserDao userDao;
    private ProductService productService;

    @Override
    protected void service(HttpServletRequest request, HttpServletResponse resp) throws IOException, ServletException {
        try (ClassPathXmlApplicationContext context =
                     new ClassPathXmlApplicationContext("applicationContext.xml")) {

            userDao = context.getBean("userDao", UserDao.class);
            orderProductDao = context.getBean("orderProductDao", OrderProductDao.class);
            orderDao = context.getBean("orderDao", OrderDao.class);
            productService = context.getBean("productService", ProductService.class);
        }

        session = request.getSession();
        String userName = (String) session.getAttribute("userName");
        user = userDao.getUserByName(userName);
        order = orderDao.getByUserId(user.getId());
        String chosenProducts = productService.getChosenProducts(orderProductDao.getChosenList(user.getId()));
        String productList = productService.getProductList();
        session.setAttribute("chosenProducts", chosenProducts);
        session.setAttribute("productList", productList);

        String method = request.getMethod();
        if (method.equals("GET")) {
            doGet(request, resp);
        } else if (method.equals("POST")) {
            doPost(request, resp);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("shopPages/catalog.jsp");
        dispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        product = new Product(request.getParameter("productOrder"));

        if (request.getParameter("addItem") != null) {
            orderProductDao.add(order.getId(), product.getId());
            response.setIntHeader("Refresh", 1);
        }
        if (request.getParameter("submit") != null) {
            orderDao.updateTotalPrice(user.getId());
            session.setAttribute("userId", user.getId());
            response.sendRedirect("/online-shop/order");
        }
    }
}
