package by.training.homework20.service;

import by.training.homework20.entity.Product;
import by.training.homework20.dao.ProductDao;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.sql.*;

public class FakeProductRepository {

    @Autowired
    private ProductDao productDao;

    private static final String CREATE_USERS_TABLE_SQL_QUERY =
            " CREATE TABLE IF NOT EXISTS Users (\n" +
                    "id INTEGER NOT NULL AUTO_INCREMENT,\n" +
                    "name VARCHAR(50) NOT NULL,\n" +
                    "password VARCHAR(500),\n" +
                    "PRIMARY KEY (id)\n" +
                    ");\n\n";

    private static final String CREATE_ORDERS_TABLE_SQL_QUERY =
            " CREATE TABLE IF NOT EXISTS Orders (\n" +
                    "id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,\n" +
                    "user_id INTEGER NOT NULL,\n" +
                    "TOTAL_price DECIMAL NOT NULL,\n" +
                    "FOREIGN KEY (user_id) REFERENCES Users (id)\n" +
                    ");\n\n";

    private static final String CREATE_PRODUCTS_TABLE_SQL_QUERY =
            " CREATE TABLE IF NOT EXISTS Products (\n" +
                    "id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,\n" +
                    "title VARCHAR(250) NOT NULL,\n" +
                    "price DECIMAL NOT NULL\n" +
                    ");\n\n";

    private static final String CREATE_ORDERS_RODUCTS_TABLE_SQL_QUERY =
            " CREATE TABLE IF NOT EXISTS Orders_Products (\n" +
                    "id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,\n" +
                    "order_id INTEGER NOT NULL,\n" +
                    "product_id INTEGER NOT NULL,\n" +
                    "FOREIGN KEY (order_id) REFERENCES Orders (id),\n" +
                    "FOREIGN KEY (product_id) REFERENCES Products (id)\n" +
                    ");\n\n";

    public void createTables() {

        String sqlQuery = CREATE_USERS_TABLE_SQL_QUERY +
                CREATE_ORDERS_TABLE_SQL_QUERY +
                CREATE_PRODUCTS_TABLE_SQL_QUERY +
                CREATE_ORDERS_RODUCTS_TABLE_SQL_QUERY;

        try (Connection connection = LocalConnectDb.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery)) {

            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void addProductsToTable() {
        Product product1 = new Product("Book", BigDecimal.valueOf(5.36));
        Product product2 = new Product("Notebook", BigDecimal.valueOf(1.27));
        Product product3 = new Product("Pen", BigDecimal.valueOf(0.53));
        Product product4 = new Product("Rule", BigDecimal.valueOf(0.21));

        productDao.add(product1);
        productDao.add(product2);
        productDao.add(product3);
        productDao.add(product4);
    }
}
