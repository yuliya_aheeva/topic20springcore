package by.training.homework20.service;

import by.training.homework20.entity.Product;
import by.training.homework20.dao.ProductDao;
import by.training.homework20.repository.ProductRepository;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ProductService {

    private ProductDao productDao;

    public ProductService() {
        this.productDao = new ProductRepository();
    }

    public String getProductList() {
        StringBuilder productStringBuilder = new StringBuilder();
        List<Product> products = productDao.getAllProducts();
        for (Product product : products) {
            productStringBuilder.append("<option>" + product.toString() + "</option>");
        }
        return productStringBuilder.toString();
    }

    public String getChosenProducts(List<Product> products) {
        StringBuilder chosenProducts = new StringBuilder();
        Set<String> setProducts = new HashSet<>();
        for (Product product : products) {
            chosenProducts.append(product.toString());
            setProducts.add(product.getTitle());
        }
        StringBuilder productsInCart = new StringBuilder();
        for (String product : setProducts) {
            Pattern pattern = Pattern.compile(product);
            Matcher matcher = pattern.matcher(chosenProducts.toString());
            int count = 0;
            while (matcher.find()) {
                count++;
            }
            productsInCart.append("<li>" + product + "   (" + count + "pcs.)</li>");
        }
        return productsInCart.toString();
    }
}
