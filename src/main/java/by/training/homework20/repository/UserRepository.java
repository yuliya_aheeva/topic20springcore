package by.training.homework20.repository;

import by.training.homework20.dao.UserDao;
import by.training.homework20.entity.User;
import by.training.homework20.service.LocalConnectDb;

import java.sql.*;

public class UserRepository implements UserDao {

    @Override
    public void add(User user) {
        String sqlQuery = "INSERT INTO Users (name, password) VALUES (?, ?)";

        try (Connection connection = LocalConnectDb.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery)) {

            preparedStatement.setString(1, user.getName());
            preparedStatement.setString(2, user.getPassword());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public User getUserByName(String userName) {

        User user = new User();

        String sqlQuery = "SELECT id, name, password FROM Users WHERE name = ?";

        try (Connection connection = LocalConnectDb.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery)) {

            preparedStatement.setString(1, userName);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                user.setId(resultSet.getLong("id"));
                user.setName(resultSet.getString("name"));
                user.setPassword(resultSet.getString("password"));
            }
            preparedStatement.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user;
    }

    @Override
    public boolean isUserExist(String userName) {

        boolean isUserExist = false;

        String sqlQuery = "SELECT id, name, password FROM Users WHERE name = ?";

        try (Connection connection = LocalConnectDb.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery)) {

            preparedStatement.setString(1, userName);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                isUserExist = true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return isUserExist;
    }
}
