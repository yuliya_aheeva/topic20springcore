package by.training.homework20.repository;

import by.training.homework20.dao.OrderDao;
import by.training.homework20.entity.Order;
import by.training.homework20.service.LocalConnectDb;

import java.sql.*;

public class OrderRepository implements OrderDao {

    @Override
    public void add(Order order) {

        String sqlQuery = "INSERT INTO Orders (user_id, total_price) " +
                "VALUES (?, 0)";

        try (Connection connection = LocalConnectDb.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery)) {

            preparedStatement.setLong(1, order.getUserId());

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateTotalPrice(long userId) {
        String sqlQuery = " UPDATE Orders \n" +
                "SET total_price = \n" +
                "(SELECT SUM(Products.price) \n" +
                "FROM Products \n" +
                "INNER JOIN Orders_Products \n" +
                "ON Orders_Products.product_id = Products.id \n" +
                "INNER JOIN Orders \n" +
                "ON Orders_Products.order_id = Orders.id \n" +
                "INNER JOIN Users \n" +
                "ON Orders.user_id = Users.id \n" +
                "and Users.id = ? )";

        try (Connection connection = LocalConnectDb.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery)) {

            preparedStatement.setLong(1, userId);

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Order getByUserId(long userId) {

        Order order = new Order();

        String sqlQuery = "SELECT id, user_id, total_price FROM Orders WHERE user_id = ?";

        try (Connection connection = LocalConnectDb.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery)) {

            preparedStatement.setLong(1, userId);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                order.setId(resultSet.getLong("id"));
                order.setUserId(resultSet.getLong("user_id"));
                order.setTotalPrice(resultSet.getBigDecimal("total_price"));
            }
            preparedStatement.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return order;
    }
}
