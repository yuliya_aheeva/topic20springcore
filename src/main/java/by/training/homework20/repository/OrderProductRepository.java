package by.training.homework20.repository;

import by.training.homework20.dao.OrderProductDao;
import by.training.homework20.entity.Product;
import by.training.homework20.service.LocalConnectDb;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class OrderProductRepository implements OrderProductDao {

    @Override
    public void add(long orderId, long productId) {

        String sqlQuery = "INSERT INTO Orders_Products (order_id, product_id) " +
                "VALUES (?, ?) ";

        try (Connection connection = LocalConnectDb.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery)) {

            preparedStatement.setLong(1, orderId);
            preparedStatement.setLong(2, productId);

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public double getTotalPrice(long userId) {
        double totalPrice = 0;
        String sqlQuery = "SELECT total_price " +
                "FROM Orders " +
                "WHERE user_id = " + userId;

        try (Connection connection1 = LocalConnectDb.getConnection();
             Statement preparedStatement = connection1.createStatement()) {

            ResultSet resultSet = preparedStatement.executeQuery(sqlQuery);
            while (resultSet.next()) {
                totalPrice = resultSet.getDouble("total_price");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return totalPrice;
    }

    @Override
    public List<Product> getChosenList(long userId) {

        List<Product> productList = new ArrayList<>();

        String sqlQuery = "SELECT p.id, p.title, p.price \n" +
                "FROM Users u \n" +
                "INNER JOIN Orders o \n" +
                "ON u.id = ? and \n" +
                "o.user_id = u.id  \n" +
                "INNER JOIN Orders_Products op \n" +
                "on op.order_id = o.id \n" +
                "INNER JOIN Products p \n" +
                "on p.id = op.product_id; \n";

        try (Connection connection = LocalConnectDb.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery)) {

            preparedStatement.setLong(1, userId);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Product product = new Product();
                product.setId(resultSet.getLong("id"));
                product.setTitle(resultSet.getString("title"));
                product.setPrice(resultSet.getBigDecimal("price"));

                productList.add(product);
            }
            preparedStatement.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return productList;
    }
}
