package by.training.homework20.dao;

import by.training.homework20.entity.Order;

public interface OrderDao {

    void add(Order order);

    Order getByUserId(long userId);

    void updateTotalPrice(long userId);

}
