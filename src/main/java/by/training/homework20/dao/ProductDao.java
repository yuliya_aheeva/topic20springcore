package by.training.homework20.dao;

import by.training.homework20.entity.Product;

import java.util.List;

public interface ProductDao {

    void add(Product product);

    List<Product> getAllProducts();

}
