<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
	<title>Total order</title>
</head>
<body>
	<div style="margin: 40px; margin-left: 100px;">
		<h1>Dear <%= (String) session.getAttribute("userName") %>, your order:</h1><br>
		<ul align="left">
			<%= (String) session.getAttribute("chosenProducts") %>
		</ul><br>
		<p align="left">Total price: $<%= request.getAttribute("totalPrice") %></p>
	</div>
</body>
</html>