<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ page import = "by.training.homework20.service.ProductService"%>

<html>
<head>
	<title>Make your order</title>
</head>
<body>
	<div style="margin: 40px; margin-left: 100px;">
	<h2>Hello <%= (String) session.getAttribute("userName") %>!</h2>
	<p>Make your order</p>
	<p>You have already chosen:</p>
	<form method="POST">
		<ul>
            <%= (String) session.getAttribute("chosenProducts")%>
		</ul>
		<select name="productOrder" >
			<%= (String) session.getAttribute("productList") %>
		</select><br></br>
		<input type="submit" name="addItem" value="Add item">  <input type="submit" name="submit" value="Submit">
	</form>
	</div>
</body>
</html>